#!/usr/bin/env bash

set -e

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_DIR/../config/vars.sh"

main() {
    local ssh_file=$HOME/.ssh/config
    local cmd="ssh"
    if [ -n "$SSH_CONFIG_FILE" ]; then
        if [ ! -f "$SSH_CONFIG_FILE" ]; then
            echo "$SSH_CONFIG_FILE file does not exist" >> $TMUX_SSH_LOG
        fi
        ssh_file="$SSH_CONFIG_FILE"
    else
        if [ ! -f "$HOME/.ssh/config" ]; then
            echo "$HOME/.ssh/config file does not exist" >> $TMUX_SSH_LOG
        fi
        ssh_file="$HOME/.ssh/config"
    fi

    cmd+=" -F $ssh_file"
    local hosts="$(sed -rn 's/^\s*Host\s+(.*)\s*/\1/ip' "$ssh_file")"
    local read_host_config="sed -n '/^Host {}$/, /^$/p' $SSH_CONFIG_FILE | bat -l ssh_config --color=always --style=changes,grid --terminal-width=80"
    local choice_with_fzf
    choice_with_fzf="$(echo -e "$hosts" | fzf --preview="$read_host_config")"

    if [ $? -gt 0 ]; then
        exit 0
    fi
    cmd+=" $choice_with_fzf"

    tmux rename-window \
        -t "${TMUX_SSH_TITLE_ICON} ${TMUX_SSH_TITLE}" \
        "${TMUX_SSH_TITLE_ICON} ${choice_with_fzf}"

    ${cmd}
}

main
