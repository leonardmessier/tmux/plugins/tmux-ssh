#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CURRENT_DIR}/config/vars.sh"

set_normal_bindings() {
    tmux bind-key -T prefix "${TMUX_SSH_DEFAULT_KEY}" switch-client -T tmux_ssh_table_1

    tmux bind-key -T tmux_ssh_table_1 "${TMUX_SSH_CONNECT_KEY}" \
        run "tmux new-window -n '${TMUX_SSH_TITLE_ICON} ${TMUX_SSH_TITLE}' -a -c '#{pane_current_path}' -S ${CURRENT_DIR}/scripts/tmux-ssh-connect.sh"
}

main() {
    set_normal_bindings
}

main
