#!/usr/bin/env bash

TMUX_SSH_DEFAULT_KEY="S"
TMUX_SSH_TITLE_ICON=""
TMUX_SSH_TITLE="SSH"
TMUX_SSH_CONNECT_KEY="c"
TMUX_SSH_KEYS_KEY="k"
TMUX_SSH_LOG="$HOME/.tmux-plugins.log"
TMUX_SSH_KEYS_DIRECTORY="config.d"

eval "$(direnv export bash)" > /dev/null
